from layer import SyncLayer
from yowsup.stacks import  YowStackBuilder
from yowsup.layers import YowLayerEvent
from yowsup.layers.auth import YowAuthenticationProtocolLayer
from yowsup.layers.network import YowNetworkLayer
from yowsup.env import YowsupEnv


class YowsupSyncStack(object):
    def __init__(self, contacts):
        """
        :param contacts: list of [jid ]
        :return:
        """
        stackBuilder = YowStackBuilder()

        self._stack = stackBuilder \
            .pushDefaultLayers() \
            .push(SyncLayer) \
            .build()

        self._stack.setProp(SyncLayer.PROP_CONTACTS, contacts)
        self._stack.setProp(YowAuthenticationProtocolLayer.PROP_PASSIVE, True)
        self._stack.setProfile("558698519222")
        YowsupEnv.setEnv("android")

    def set_prop(self, key, val):
        self._stack.setProp(key, val)

    def start(self):
        self._stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
        self._stack.loop()