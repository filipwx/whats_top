from yowsup.stacks import  YowStackBuilder
from layer import EchoLayer
from yowsup.layers import YowLayerEvent
from yowsup.layers.network import YowNetworkLayer
from yowsup.env import YowsupEnv


stackBuilder = YowStackBuilder()

stack = stackBuilder\
    .pushDefaultLayers()\
    .push(EchoLayer)\
    .build()

stack.setProfile("558698519222")
YowsupEnv.setEnv("android")
stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
stack.loop()